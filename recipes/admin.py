from django.contrib import admin
from recipes.models import Recipe, RecipeStep, RecipeDetail

class RecipeDetailInline(admin.StackedInline):
    model = RecipeDetail

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    inlines = [RecipeDetailInline]


    list_display = [
        "title",
        "id",
        "get_cost",
        "get_difficulty",
        "get_nutrition",
    ]

    def get_cost(self, obj):
        return obj.recipedetail.cost

    def get_difficulty(self, obj):
        return obj.recipedetail.difficulty

    def get_nutrition(self, obj):
        return obj.recipedetail.nutrition

    get_cost.short_description = "Cost"
    get_difficulty.short_description = "Difficulty"
    get_nutrition.short_description = "Nutrition"


@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "step_number",
        "instruction",
        "id",
    )
