from django import forms
from django.forms import ModelForm
from recipes.models import Recipe, RecipeDetail


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = (
            "title",
            "picture",
            "description",
        )


class RecipeDetailForm(forms.ModelForm):
    DIFFICULTY_CHOICES = [
        ('N/A', 'N/A'),
        ('1', '1'),
        ('2', '2'),
        ('Michelin Mode', 'Michelin Mode'),
    ]

    difficulty = forms.ChoiceField(choices=DIFFICULTY_CHOICES, label='Difficulty')


    class Meta:
        model = RecipeDetail
        fields = ['cost', 'difficulty', 'nutrition']
