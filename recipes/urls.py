from django.urls import path
from recipes.views import recipe_list, show_recipe, create_recipe, edit_recipe

urlpatterns = [
    #recipes/details
    path("", recipe_list, name="recipe_list"),
    path("<int:id>/", show_recipe, name="show_recipe"),
    path("create/", create_recipe, name="create_recipe"),
    path("edit/<int:id>/", edit_recipe, name="edit_recipe"),
]

#routed to the function, which then gets to the details.
#routes to function view.
