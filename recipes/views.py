from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe, RecipeDetail
from recipes.forms import RecipeForm, RecipeDetailForm

# Create your views here.
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    #get a single recipe from the DB
    return render(request, "recipes/detail.html", context)
def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            form.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()
    context = {
        "form": form
    }
    return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST": #if someone clicks a submit button
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
          #covers incorrect data & viewing page normally

    context = {
      "form": form,
      "recipe_object": recipe,
    }
    return render(request, "recipes/edit.html", context)


def recipe_detail(request, recipe_id):
    recipe = get_object_or_404(Recipe, pk=recipe_id)
    recipe_detail, created = RecipeDetail.objects.get_or_create(recipe=recipe)

    if request.method == 'POST':
        form = RecipeDetailForm(request.POST, instance=recipe_detail)
        if form.is_valid():
            form.save()
            return redirect('recipe_detail', recipe_id=recipe.id)
    else:
        form = RecipeDetailForm(instance=recipe_detail)

    context = {
        'recipe': recipe,
        'recipe_detail': recipe_detail,
        'form': form
    }

    return render(request, 'recipe_detail.html', context)
